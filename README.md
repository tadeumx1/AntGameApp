# AntGameApp

This is a Ant Race Game App

React 18.2.0

React Native 0.71.4

Jest 29.2.1



### Stack

- React Native

### Installation

This application needs [Node.js](https://nodejs.org/) installed together with [NPM](https://www.npmjs.com/get-npm) so you will be able to install react-native-cli to run the React Native App and after this you can execute the commands below

You must have Node 16.13.1 or higher to run the React Native Application

First use the `` git clone`` after this

```
$ cd AntGameApp

$ npm install i 

// Or you can use Yarn

$ yarn install

// Commmand to run Tests

$ npm run test or yarn test

Commmand to run Test Coverage in the application

$ npm run coverage or yarn coverage

// Commmand to run React Native App

$ npm start or yarn start

$ react-native run-android or react-native run-ios

```
