import { api } from './client/api';
import { AxiosResponse } from 'axios';

export interface Ant {
  name: string;
  length: number;
  color: string;
  antWinLikehood?: number;
  antState?: string;
  weight: number;
}

export type ResponseGetAntsData = {
  ants: Ant[];
};

export function create() {
  return {
    async getAntsData() {
      return api
        .get('/ants')
        .then(({ data }: AxiosResponse<ResponseGetAntsData>) => {
          return data;
        });
    },
  };
}
