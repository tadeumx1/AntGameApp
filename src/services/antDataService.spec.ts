import { antDataService } from './';
import { api } from './client';
import MockAdapter from 'axios-mock-adapter';

const mockAPI = new MockAdapter(api);

const antsMockResponse = {
  ants: [
    { name: 'Marie Ant oinette', length: 12, color: 'BLACK', weight: 2 },
    { name: 'Flamin Pincers', length: 11, color: 'RED', weight: 2 },
    { name: 'Big Susan', length: 20, color: 'BLACK', weight: 5 },
    {
      name: 'The Unbeareable Lightness of Being',
      length: 5,
      color: 'SILVER',
      weight: 1,
    },
    { name: 'The Duke', length: 17, color: 'RED', weight: 3 },
  ],
};

describe('services/antDataservice', () => {
  describe('fetches data from endpoint', () => {
    it('should get customer result', async () => {
      mockAPI.onGet('/ants').reply(200, antsMockResponse);

      const payload = await antDataService.create().getAntsData();
      expect(payload).toEqual(antsMockResponse);
    });
  });
});
