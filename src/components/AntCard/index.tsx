import React from 'react';

import { Ant } from '../../services/antDataService';
import AntImage from '../AntImage';

import {
  Container,
  InfoContainer,
  DescriptionAnt,
  ContainerAntColorDescription,
  ColorView,
  DescriptionStateAnt,
  AntName,
} from './styles';

interface AntCardProps {
  ant: Ant;
}

const antImage =
  'https://www.pngitem.com/pimgs/m/297-2979894_ant-nest-clipart-black-ant-clipart-hd-png.png';

export default function AntCard({ ant }: AntCardProps) {
  return (
    <Container>
      <AntImage image={antImage} />
      <InfoContainer>
        <AntName>{ant.name}</AntName>
        <DescriptionAnt>Weight {ant.weight} g</DescriptionAnt>
        <DescriptionAnt>Length {ant.length} mm</DescriptionAnt>
        <ContainerAntColorDescription>
          <DescriptionAnt>Color</DescriptionAnt>
          <ColorView testID="color-ant" color={ant.color.toLowerCase()} />
        </ContainerAntColorDescription>
        <DescriptionAnt>
          Ant Likelihood {(ant.antWinLikehood || 0).toFixed(3)}
        </DescriptionAnt>
        <DescriptionStateAnt>Ant State {ant.antState}</DescriptionStateAnt>
      </InfoContainer>
    </Container>
  );
}
