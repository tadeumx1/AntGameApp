import styled from 'styled-components/native';

interface ColorAntProps {
  color: string;
}

export const Container = styled.View`
  background-color: #f7f7f7;
  shadow-color: #000;
  shadow-radius: 1px;
  shadow-opacity: 0.7;
  shadow-offset: 0px 1px;
  elevation: 3;
  width: 150px;
  border-radius: 3px;
  margin: 10px;
  padding: 5px;
`;

export const InfoContainer = styled.View`
  flex-direction: column;
  margin-top: 5px;
  margin-left: 10px;
`;

export const ContainerAntColorDescription = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ColorView = styled.View<ColorAntProps>`
  height: 10px;
  width: 10px;
  margin-top: 3px;
  margin-left: 5px;
  background-color: #000000;

  ${({ color }) =>
    `
    background-color: ${color};
  `};
`;

export const DescriptionAnt = styled.Text`
  font-family: System;
  color: #000000;
  font-size: 14px;
`;

export const DescriptionStateAnt = styled.Text`
  font-family: System;
  color: #000000;
  margin-top: 10px;
  font-size: 14px;
`;

export const AntName = styled.Text`
  font-weight: bold;
  margin-bottom: 10px;
  font-size: 14px;
`;
