import React from 'react';
import { render } from '@testing-library/react-native';
import AntCard from './index';

const antMock = {
  name: 'The Unbeareable Lightness of Being',
  length: 5,
  color: 'BLACK',
  weight: 1,
};

describe('AntCard Component', () => {
  it('should render screen correct', async () => {
    const component = render(<AntCard ant={antMock} />);
    expect(component).toMatchSnapshot();
  });

  it('should render color ant correct AntCard Component', () => {
    const { getByText, getByTestId } = render(<AntCard ant={antMock} />);

    expect(getByText('Color')).toBeDefined();

    expect(getByTestId('color-ant')).toHaveStyle({
      backgroundColor: 'black',
    });
  });

  it('should render AntCard Component', () => {
    const { getByText } = render(<AntCard ant={antMock} />);

    expect(getByText('The Unbeareable Lightness of Being')).toBeDefined();
    expect(getByText('Weight 1 g')).toBeDefined();
    expect(getByText('Length 5 mm')).toBeDefined();
    expect(getByText('Color')).toBeDefined();
    expect(getByText('Ant Likelihood 0.000')).toBeDefined();
    expect(getByText('Ant State')).toBeDefined();
  });
});
