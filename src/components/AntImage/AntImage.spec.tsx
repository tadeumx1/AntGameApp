import React from 'react';
import { render } from '@testing-library/react-native';
import AntImage from './index';

const imageMock =
  'https://www.pngitem.com/pimgs/m/297-2979894_ant-nest-clipart-black-ant-clipart-hd-png.png';

describe('AntImage Component', () => {
  it('should render screen correct', async () => {
    const component = render(<AntImage image={imageMock} />);
    expect(component).toMatchSnapshot();
  });

  it('should render AntListComponent', () => {
    const { getByTestId } = render(<AntImage image={imageMock} />);

    expect(getByTestId('image-ant')).toBeDefined();
  });
});
