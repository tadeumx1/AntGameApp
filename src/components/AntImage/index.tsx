import React from 'react';

import { Image } from './styles';

interface Props {
  image: string;
  style?: object;
}

const AntImage = ({ image, style }: Props) => {
  return (
    <Image
      source={{ uri: image }}
      style={{ ...style }}
      testID="image-ant"
      resizeMode="contain"
    />
  );
};

export default AntImage;
