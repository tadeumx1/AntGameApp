import styled from 'styled-components/native';
import { FlatList } from 'react-native';

import { Ant } from '../../services/antDataService';

export const AntListContainer = styled(FlatList as new () => FlatList<Ant>)`
  width: 100%;
  height: 300px;
  max-height: 300px;
`;

export const ContainerMessage = styled.View`
  justify-content: center;
  align-items: center;
`;

export const Title = styled.Text`
  font-family: System;
  color: #000000;
  text-align: center;
  font-size: 16px;
`;
