import React from 'react';
import { render } from '@testing-library/react-native';
import AntList from './index';

const ants = [
  { name: 'Marie Ant oinette', length: 12, color: 'BLACK', weight: 2 },
  { name: 'Flamin Pincers', length: 11, color: 'RED', weight: 2 },
  { name: 'Big Susan', length: 20, color: 'BLACK', weight: 5 },
  {
    name: 'The Unbeareable Lightness of Being',
    length: 5,
    color: 'SILVER',
    weight: 1,
  },
  { name: 'The Duke', length: 17, color: 'RED', weight: 3 },
];

describe('AntCard Component', () => {
  it('should render screen correct', async () => {
    const component = render(
      <AntList antsData={ants} loadingAntProposal={false} />
    );
    expect(component).toMatchSnapshot();
  });

  it('should render AntListComponent', () => {
    const { getByText, getAllByText } = render(
      <AntList antsData={ants} loadingAntProposal={false} />
    );

    expect(getByText('The Unbeareable Lightness of Being')).toBeDefined();
    expect(getByText('Weight 1 g')).toBeDefined();
    expect(getByText('Length 5 mm')).toBeDefined();
    expect(getAllByText('Color')[0]).toBeDefined();
    expect(getAllByText('Ant Likelihood 0.000')[3]).toBeDefined();
    expect(getAllByText('Ant State')[3]).toBeDefined();
  });

  it('should render list all items', () => {
    const { getByTestId } = render(
      <AntList antsData={ants} loadingAntProposal={false} />
    );

    expect(getByTestId('list-ant').props.data.length).toBe(5);
  });
});
