import React from 'react';
import { ListRenderItem } from 'react-native';
import { Ant } from '../../services/antDataService';

import AntCard from '../AntCard';

import { AntListContainer, Title, ContainerMessage } from './styles';

interface AntListProps {
  antsData: Ant[];
  loadingAntProposal: boolean;
}

export default function AntList({
  antsData,
  loadingAntProposal,
}: AntListProps) {
  const renderItem: ListRenderItem<Ant> = ({ item }) => <AntCard ant={item} />;

  const renderAntDescription = () => {
    return (
      <ContainerMessage>
        {!loadingAntProposal ? (
          <Title>This is the Ant Race Game please press the Button</Title>
        ) : (
          <Title>The race is on please wait</Title>
        )}
      </ContainerMessage>
    );
  };

  return (
    <AntListContainer
      // @ts-ignore
      data={antsData}
      horizontal
      ListEmptyComponent={renderAntDescription}
      testID="list-ant"
      keyExtractor={(item: Ant) => item.name}
      renderItem={renderItem}
    />
  );
}
