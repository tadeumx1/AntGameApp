import React, { useState } from 'react';
import StateView from 'react-native-easy-state-view';
// @ts-ignore
import ProgressBar from 'react-native-progress/Bar';

import {
  Container,
  Title,
  ContainerButton,
  Button,
  ButtonText,
  LoadingButton,
  DescriptionGameData,
  ContainerLoading,
  ContainerLoadingGameData,
  LoadingAntData,
  DescriptionLoading,
} from './styles';

import AntList from '../../components/AntList';
import { antDataService } from '../../services';
import { Ant } from '../../services/antDataService';

enum REQUESTS {
  GET_ANT_DATA = 'GET_ANT_DATA',
}

export default function Home() {
  const [antData, setAntData] = useState<Array<Ant>>([]);
  const [errorFeedback, setErrorFeedback] = useState('');
  const [loading, setLoading] = useState(false);
  const [dataGameResult, setDataGameResult] = useState(false);
  const [loadingGameData, setLoadingGameData] = useState(false);

  const handleButton = async () => {
    if (!antData.length) {
      await handleGetAntData();
    } else {
      const dataAnt = antData.map(item => {
        return {
          ...item,
          antWinLikehood: 0,
          antState: 'In Progress',
        };
      });

      setAntData(dataAnt);

      setDataGameResult(false);
      await handleAntGameData();
    }
  };

  const handleButtonTitle = () => {
    if (!antData.length) {
      return 'Fetch Ant Data';
    } else {
      return 'Play Game';
    }
  };

  function generateAntWinLikelihoodCalculator() {
    const delay = 7000 + Math.random() * 7000;
    const likelihoodOfAntWinning = Math.random();

    return (callback: (arg: number) => void) => {
      setTimeout(() => {
        callback(likelihoodOfAntWinning);
      }, delay);
    };
  }

  const generateAntWinLikelihoodCalculatorPromise = (): Promise<number> => {
    return new Promise<number>(resolve => {
      const result = generateAntWinLikelihoodCalculator();
      result(value => {
        resolve(value);
      });
    });
  };

  const handleAntGameData = async () => {
    const resultAntData = antData.map(async (item): Promise<Ant> => {
      const resultItem: number =
        await generateAntWinLikelihoodCalculatorPromise();

      return {
        ...item,
        antState: 'All Calculated',
        antWinLikehood: resultItem,
      };
    });

    setLoadingGameData(true);
    const results = await Promise.all(resultAntData);

    const resultArrayLikeHood = results;
    resultArrayLikeHood.sort(
      (a, b) => (b.antWinLikehood || 0) - (a.antWinLikehood || 0)
    );

    setAntData(resultArrayLikeHood);
    setDataGameResult(true);
    setLoadingGameData(false);
  };

  const handleGetAntData = async () => {
    setErrorFeedback('');
    setLoading(true);

    await antDataService
      .create()
      .getAntsData()
      .then(data => {
        const dataAnt = data.ants.map(item => {
          return {
            ...item,
            antState: 'Not yet run',
          };
        });

        setAntData(dataAnt);
      })
      .catch(() => setErrorFeedback(REQUESTS.GET_ANT_DATA))
      .finally(() => setLoading(false));
  };

  const retry = () => {
    switch (errorFeedback) {
      case REQUESTS.GET_ANT_DATA:
        return handleGetAntData();
    }
  };

  if (errorFeedback) {
    return (
      <StateView
        enableButton
        buttonText="Try again"
        title="A problem here"
        description="A connection error occurred"
        onPress={retry}
      />
    );
  }

  return (
    <Container>
      <ContainerButton>
        <Title>Ant Game</Title>
        {dataGameResult && (
          <DescriptionGameData>
            The race ended see the results
          </DescriptionGameData>
        )}
        {loading ? (
          <ContainerLoading>
            <DescriptionLoading>Fetching Ant Data</DescriptionLoading>
            <LoadingAntData />
          </ContainerLoading>
        ) : (
          <AntList loadingAntProposal={loadingGameData} antsData={antData} />
        )}
        {loadingGameData && (
          <ContainerLoadingGameData>
            <ProgressBar indeterminate={true} color="#000000" />
          </ContainerLoadingGameData>
        )}
      </ContainerButton>
      <Button disabled={loading || loadingGameData} onPress={handleButton}>
        {loading || loadingGameData ? (
          <LoadingButton />
        ) : (
          <ButtonText>{handleButtonTitle()}</ButtonText>
        )}
      </Button>
    </Container>
  );
}
