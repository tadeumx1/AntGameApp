import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: #fff;
  padding: 20px;
`;

export const ContainerButton = styled.View`
  flex: 1;
  background-color: #fff;
`;

export const Title = styled.Text`
  font-family: System;
  margin: 24px 24px 0px;
  color: #000000;
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 50px;
`;

export const DescriptionGameData = styled.Text`
  font-family: System;
  color: #000000;
  margin: 10px 0px;
  font-size: 20px;
`;

export const LoadingAntData = styled.ActivityIndicator.attrs({
  size: 'small',
  color: '#000000',
})``;

export const ContainerLoadingGameData = styled.View`
  width: 100%;
  margin: 20px 0px;
  align-items: center;
  justify-content: center;
`;

export const DescriptionLoading = styled.Text`
  color: #000000;
  margin-right: 10px;
  font-size: 20px;
`;

export const ContainerLoading = styled.View`
  flex-direction: row;
  width: 100%;
  height: 300px;
  max-height: 300px;
  align-items: center;
  justify-content: center;
`;

export const ButtonText = styled.Text`
  color: #ffffff;
  font-weight: bold;
  font-size: 14px;
`;

export const LoadingButton = styled.ActivityIndicator.attrs({
  size: 'small',
  color: '#FFFFFF',
})``;

export const Button = styled.TouchableOpacity`
  background-color: #7a91ca;
  border-radius: 3px;
  height: 40px;
  padding: 0px 20px;
  justify-content: center;
  align-items: center;
`;
