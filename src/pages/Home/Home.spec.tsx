import React from 'react';
import { render } from '@testing-library/react-native';
import Home from './index';
import { antDataService } from '../../services';

const antsMockResponse = {
  ants: [
    { name: 'Marie Ant oinette', length: 12, color: 'BLACK', weight: 2 },
    { name: 'Flamin Pincers', length: 11, color: 'RED', weight: 2 },
    { name: 'Big Susan', length: 20, color: 'BLACK', weight: 5 },
    {
      name: 'The Unbeareable Lightness of Being',
      length: 5,
      color: 'SILVER',
      weight: 1,
    },
    { name: 'The Duke', length: 17, color: 'RED', weight: 3 },
  ],
};

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    ...jest.requireActual('@react-navigation/native'),
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
    useIsFocused: jest.fn(() => true),
  };
});

/* jest.mock('react-native-easy-state-view');
const StateViewMocked = StateView as jest.Mock;
StateViewMocked.mockImplementation(() => {
  return (
    <View>
      <Text>State View</Text>
    </View>
  );
}); */

describe('Home Component', () => {
  it('should render screen correct', async () => {
    jest.spyOn(antDataService, 'create').mockImplementation(() => {
      return {
        getAntsData: () => Promise.resolve(antsMockResponse),
      };
    });

    const component = render(<Home />);

    expect(component).toMatchSnapshot();
  });
});
